//PNG Fallback

if (!Modernizr.svg) {
    var images = $('img[data-png-fallback]');
    images.each(function(i) {
        $(this).attr('src', $(this).data('png-fallback'));
    });
}

//Toggle Boxes
$(document).ready(function() {
    $('body').addClass('js');
    var $activatelink = $('.activate-link');

    $activatelink.click(function() {
        var $this = $(this);
        $this.toggleClass('active').next('div').toggleClass('active');
        return false;
    });

});

//Responsive Navigation
$(document).ready(function() {
    $('body').addClass('js');
    var $menu = $('.site-nav-container'),
        $menulink = $('.menu-link'),
        $menuTrigger = $('.menu-item-has-children > a'),
        $searchLink = $('.search-link'),
        $siteSearch = $('.search-module'),
        $siteWrap = $('.site-wrap');

    $searchLink.click(function(e) {
        e.preventDefault();
        $searchLink.toggleClass('active');
        $siteSearch.toggleClass('active');
    });

    $menulink.click(function(e) {
        e.preventDefault();
        $menulink.toggleClass('active');
        $menu.toggleClass('active');
        $siteWrap.toggleClass('nav-active');
    });

    $menuTrigger.click(function(e) {
        //e.preventDefault();
        var $this = $(this);
        $this.toggleClass('active').next('ul').toggleClass('active');
    });

});

//Lightbox
$(document).ready(function() {
    $('.lightbox').magnificPopup({ type: 'image' });
});
$(document).ready(function() {
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });
});



//Show More
$(document).ready(function() {
    $(".showmore").after("<p><a href='#' class='show-more-link'>More</a></p>");
    var $showmorelink = $('.showmore-link');
    $showmorelink.click(function() {
        var $this = $(this);
        var $showmorecontent = $('.showmore');
        $this.toggleClass('active');
        $showmorecontent.toggleClass('active');
        return false;
    });
});

//Show More
$(document).ready(function() {
    var $expandlink = $('.headexpand');
    $expandlink.click(function() {
        var $this = $(this);
        var $showmorecontent = $('.showmore');


        $this.toggleClass('active').next().toggleClass('active');
        $showmorecontent.toggleClass('active');
        return false;
    });
});


//Flexslider    
$(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: false,
        controlNav: false,
        itemWidth: 220,
        itemMargin: 10,
        minItems: 1,
        maxItems: 3,
        touch: true
    });
});

/*
//Sticky Nav
$(function() {
 
    // grab the initial top offset of the navigation 
    var sticky_navigation_offset_top = $('.nav-wrap').offset().top;
     
    // our function that decides weather the navigation bar should have "fixed" css position or not.
    var sticky_navigation = function(){
        var scroll_top = $(window).scrollTop(); // our current vertical position from the top
         
        // if we've scrolled more than the navigation, change its position to fixed to stick to top,
        // otherwise change it back to relative
        if (scroll_top > sticky_navigation_offset_top) { 
            $('.nav-wrap').addClass('nav-sticky');
        } else {
            $('.nav-wrap').removeClass('nav-sticky'); 
        }   
    };
     
    // run our function on load
    sticky_navigation();
     
    // and run it again every time you scroll
    $(window).scroll(function() {
         sticky_navigation();
    });
 
});



*/


// //Slide in CTA
// $(function() {
//     var slidebox = $('#slidebox');
//     if (slidebox) {
//         $(window).scroll(function() {
//             var distanceTop = $('#last').offset().top - $(window).height();
//             if ($(window).scrollTop() > distanceTop)
//                 slidebox.animate({ 'right': '0px' }, 300);
//             else
//                 slidebox.stop(true).animate({ 'right': '-430px' }, 100);
//         });
//         $('#slidebox .close').on('click', function() {
//             $(this).parent().remove();
//         });
//     }
// });

// Capabilities Functionality
$('.cr-nav-item').click(function() {
    var screenSize = $(window).width();
    if (screenSize <= '640') {
        return true;
    } else {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        // get & set title
        var title = $(this).html();
        $('.cr-subtitle').html(title);
        // get & set background image
        var imgInfo = $(this).data("img");
        $('.cr-window').css('background-image', "url(" + imgInfo + ")");
        // get & set the blurb
        var blurbInfo = $(this).data("blurb");
        $('.cr-blurb').html(blurbInfo);
        // get and set the link
        var linkInfo = $(this).attr("href");
        $('.cr-link').attr('href', linkInfo);
        return false;
    }
});

// Show and hide chart
$('.table-title').click(function() {
    $(this).toggleClass('active');
    $(this).next().toggleClass("active");
});

// smooth scroll to spot
$(function() {
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});

//Loads new images each page refresh on home page
$(window).load(function() {
    var images = ['site-intro-bg-1.png', 'site-intro-bg-2.png', 'site-intro-bg-3.png'];
    mediaCheck({
        media: '(min-width: 640px)',
        entry: function() {
            $('.site-intro-section').css({ 'background-image': 'url(https://unitedlens.com/assets/' + images[Math.floor(Math.random() * images.length)] + ')' });
        },
        exit: function() {
            $('.site-intro-section').css({ 'background-image': 'none' });

        },

    });

});

$('.sh-lang').click(function() {
    $(".google-translator").toggleClass("active");
    return false;
});

$('.close-gt').click(function() {
    $(".google-translator").toggleClass("active");
    return false;
});
