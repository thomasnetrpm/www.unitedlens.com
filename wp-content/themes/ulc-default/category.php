<?php
/**
 * The template for displaying Category Archive pages
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
<section class="site-content" role="main">
    <div class="inner-wrap">
		<?php if ( have_posts() ): ?>
		<h1><?php echo single_cat_title( '', false ); ?></h1>
		<article class="site-content-primary col-9">

		<?php while ( have_posts() ) : the_post(); ?>
<hr>
				
<article class="row">
            <figure class="col-3"><a href="<?php esc_url( the_permalink() ); ?>"><?php the_post_thumbnail('medium'); ?></a></figure>
            <div class="col-9">
            <h3><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
            <div class="post-meta"><time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time> | <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?> </div>
				<?php the_excerpt(); ?>
				
				<?php comments_template( '', true ); ?>

           </div>
        </article>
		<?php endwhile; ?>

		<?php else: ?>
		<h2>No posts to display in <?php echo single_cat_title( '', false ); ?></h2>
		<?php endif; ?>
		<?php wp_pagenavi(); ?>
	</article>	
	<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar-blog' ) ); ?>
	</div>
</section>
<?php Starkers_Utilities::get_template_parts( array( 'parts/featured-resources' ) ); ?>  
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>