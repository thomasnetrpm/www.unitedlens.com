   

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<!--Site Content-->
	<section class="site-content" role="main">
    
			
<div class="woocommerce-content">


	    

	        	   		<?php if ( is_shop() ) { 
                     //For ANY product archive.
   //Product taxonomy, product search or /shop landing
    woocommerce_get_template( 'archive-product.php' );
   
    }elseif( is_product()){ ?>
    <div class="inner-wrap">
<article> 
  <?php 
     woocommerce_content();
     ?>

</article> 

  </div>
   <?php Starkers_Utilities::get_template_parts( array( 'parts/shop-cta-module' ) ); ?>
<?php  } else{ ?>
<div class="inner-wrap">
<article> 
  <?php 
     woocommerce_content();
     ?>

</article>
  </div>
  <?php if(is_product_category()):?>  
<?php Starkers_Utilities::get_template_parts( array( 'parts/product-cta-module' ) ); ?>
<?php endif;?>
  <?php Starkers_Utilities::get_template_parts( array( 'parts/product-categories-module' ) ); ?>

 <?php Starkers_Utilities::get_template_parts( array( 'parts/shop-cta-module' ) ); ?>  
  <?php } ?> 

</div>
</section>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>