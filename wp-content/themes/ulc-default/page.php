<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
	<section class="site-content" role="main">
	    <div class="inner-wrap">

	        <article class="site-content-primary col-9"> 

	        	<h1 class="scp-title">
                    <?php if(get_field('alternative_h1')){
                        echo get_field('alternative_h1');
                    }
                    else {
                        the_title();
                    }
                    ?>
                </h1>
           
	       		<?php the_content(); ?>  


				<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
				<?php if (is_page( '608' )) : ?>
					<!--Sitemap Page-->
				    <ul>
				    <?php
				    // Add pages you'd like to exclude in the exclude here
				    wp_list_pages(
				    array(
				    'exclude' => '',
				    'title_li' => '',
				    )
				    );
				    ?>
				    </ul>

				<?php elseif (is_page( '6' )) : ?>
        			<div class="product-item-wrap rows-of-3-ns">
                        <a href="<?php bloginfo('url'); ?>/industries-aerospace/" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/indust-aerospace.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Aerospace</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/industries-astronomical/" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/indust-astronomical.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Astronomical</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/industries-fiber-optics/" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/indust-fiber.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Fiber Optics</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/industries-industrial-commercial/" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/indust-industrial.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Industrial / Commercial</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/industries-medical-bioscience" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/indust-medical.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Medical / Bioscience</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/industries-laser/" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/indust-laser.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Laser</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/industries-military-defense" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/indust-military.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Military / Defense</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/industries-ophthalmic" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/indust-ophthalmic.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Ophthalmic</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/industries-telecommunication" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/indust-telecom.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Telecommunication</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                    </div>

                <?php elseif (is_page( '352' )) : ?><!--Materials Page -->

                    <div class="product-item-wrap rows-of-3-ns">
                        
                        <a href="<?php bloginfo('url'); ?>/materials-low-expansion-glass" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/low-expansion.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Low Expansion Glass</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/materials-fused-silica/" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/fused-silica.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Fused Silica</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/materials-fused-quartz/" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/fused-quartz.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Fused Quartz</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/materials-filter-glass" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/filter-collage.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Filter Glass</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/materials-optical-glass" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/optical-glass-img.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Optical Glass</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                    </div>
                    

      	        <?php elseif (is_page( '426' )) : ?><!--Galleries Page -->
                    <div class="product-item-wrap rows-of-3-ns">
                        <a href="<?php bloginfo('url'); ?>/galleries-blank-precision-machining/" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/BlankPrecision.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Blank Precision Machining</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/galleries-grinding-polishing" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/PolishingGrinding.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Polishing</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/galleries-thin-film-coating" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/ThinFilm.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Thin Film Coating</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a> 
                        <a href="<?php bloginfo('url'); ?>/galleries-custom-hand-molding" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/HandMolding.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Custom Hand Molding</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        <a href="<?php bloginfo('url'); ?>/galleries-quality-assurance" class="product-item">
                        <figure class="">
                            <img src="<?php bloginfo('template_url'); ?>/img/Quality.jpg" alt="img10"/>
                            <figcaption>
                                <h3>Quality Assurance</h3>
                                <p>Learn More</p>
                            </figcaption>           
                        </figure>
                        </a>
                        
                    </div>
						

				<?php endif; ?>                    
	        </article>
	        
	        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar','parts/shared/flexible-content-fullwidth' ) ); ?>            
		</div>

	</section>
		<a class="back-to-top" href="#search">Back to Top</a>
    <?php if ('5' == $post->post_parent) : ?>
    <?php else : ?>   
        
    <?php endif; ?>   

<?php endwhile; ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/featured-resources' ) ); ?>       

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox' ) ); ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>