</section><!--site-content-wrap END-->


<!--Site Footer-->
<footer class="site-footer" role="contentinfo">
    <div class="inner-wrap">
        <ul>
            <li class="sf-col">
                <h4 class="sf-title">Contact United Lens Company</h4>
                <p class="sf-address">259 Worcester St. Southbridge, MA 01550</p>
                <p class="sf-phone"><a href="tel:508-765-5421" rel="nofollow">(508) 765-5421</a></p>
                <p class="sf-email"><a href="mailto:info@unitedlens.com" rel="nofollow">Info@UnitedLens.com</a></p>
                
            </li>
            <li class="sf-col">
                <h4 class="sf-title">Resources</h4>
                <p class="sf-resource"><a href="http://info.unitedlens.com/debris-shields-manufacturing-and-procurement" rel="nofollow" target="_blank">Guide to Debris Shield Manufacturing & Procurement</a></p>
                <p class="sf-resource"><a href="http://info.unitedlens.com/pioneering-in-polishing-with-optipro" rel="nofollow" target="_blank">Guide to Polishing with OptiPro Pro 160P</a></p>
            </li>
            <li class="sf-col">
                <!-- <h4 class="sf-title">Twitter Feed</h4>
                <script type="text/javascript" src="https://output12.rssinclude.com/output?type=js&amp;id=987010&amp;hash=1de0fba807bc321a72968a964b43d7c2"></script> -->
                <h4 class="sf-title">Stay In Touch!</h4>
                <div class="social-wrap">
                    <a href="https://twitter.com/UnitedLens" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-twitter.png" alt="Twitter"></a>
                    <a href="https://www.linkedin.com/company/united-lens-company-inc-?trk=company_logo" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-linkedin.png" alt="Linked In"></a>
                    <a href="https://plus.google.com/u/0/118007641060478605422/posts" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-google-plus.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-google-plus.png" alt="Google Plus"></a>
                    <a href="https://www.youtube.com/user/unitedlens2012" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-youtube.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-youtube.png" alt="Youtube"></a>
                </div>
            </li>

        </ul>
    </div>
    <div class="sf-small">
        <div class="inner-wrap">
            <p>&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?> | <a href="/privacy-policy">Privacy Policy</a>  |  <a href="/site-map">Sitemap</a>  |  Website by <a href="http://business.thomasnet.com/rpm" target="_blank">ThomasNet RPM</a></p>
        </div>
    </div>
</footer>

