<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<title><?php wp_title( '|' ); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico"/>
		<?php wp_head(); ?>
		<!--[if lt IE 9]>
            <script src="<?php bloginfo('template_url'); ?>/js/vendor/respond.min.js"></script> <script src="<?php bloginfo('template_url'); ?>/js/vendor/selectivizr-min.js"></script>
        <![endif]-->
    <meta name="google-translate-customization" content="bfe7eeff68af2593-e4f72112a27e6ccd-gf2cab25b44fc803d-12"></meta>

		<script type="text/javascript">
		var google_replace_number="(508)-765-5421";
		(function(a,e,c,f,g,b,d){var h={ak:"967682840",cl:"gQeACPT7umMQmNa2zQM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[f]||(a[f]=h.ak);b=e.createElement(g);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(g)[0];d.parentNode.insertBefore(b,d);a._googWcmGet=function(b,d,e){a[c](2,b,h,d,null,new Date,e)}})(window,document,"_googWcmImpl","_googWcmAk","script");
		</script>

	</head>
	<body <?php body_class(); ?>>
	
	<!-- Google Tag Manager --> 
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KNPN8X" 
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> 
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': 
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], 
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); 
	})(window,document,'script','dataLayer','GTM-KNPN8X');</script> 
	<!-- End Google Tag Manager -->
