<!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
<div class="site-wrap">
    
<!--Site Search -->
<div id="search" class="search-module">
    <div class="inner-wrap">
        <a href="#" target="_blank" class="search-link search-exit active"><img src="<?php bloginfo('template_url'); ?>/img/ico-exit.svg" alt="Exit"></a>
        <?php get_search_form(); ?>
    </div>
</div>
<!--Site Header-->
<div class="google-translator"> 
    <a href="#" class="close-gt">Close</a>
    <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}

</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<?php if(get_field('emergency_message')){ ?>

    <section class="emergency-notification">
        <div class="inner-wrap"> 
            <?php echo get_field('emergency_message'); ?>
        </div>
    </section>

<?php } ?>
<header class="site-header" role="banner">
    <div class="inner-wrap">
         <a href="<?php bloginfo('url'); ?>" class="site-logo"><img src="<?php bloginfo('template_url'); ?>/img/ulc-logo-1.png" alt="ULC Logo"><span>“The Symbol of Quality, Value and Integrity”</span></a>
        <div class="site-utility-nav">
            <div class="sh-utility-menu">

                <div class="sh-utility-tab">
                    <a href="<?php bloginfo('url'); ?>">Home</a>
                    <a href="<?php bloginfo('url'); ?>/about-united-lens">About Us</a>
                    <a class="sh-lang" href="#" rel="nofollow">Language</a>
                    <a href="http://info.unitedlens.com/contact-united-lens-company" target="_blank">Contact</a>
                    <a href="http://info.unitedlens.com/careers">Careers</a> 
                </div>
                <span class="sh-creds sh-itar" >Registered</span><a class="sh-creds sh-iso" href="http://info.unitedlens.com/ulc-iso-certification"  rel="nofollow" target="_blank"> 9001:2008</a>
            </div>
            <a href="tel:508-765-5421" class="sh-ph" rel="nofollow"><span class="google-number">(508)-765-5421</span></a> 
            <span class="sh-icons">
                <a href="tel:508-765-5421" class="sh-ico-contact" target="_blank" rel="nofollow"><span>Contact</span></a>  
                <a href="#menu" class="sh-ico-menu menu-link" ><span>Menu</span></a>
            </span> 
        </div> 
        <!--Site Nav-->
        <div class="site-nav-container">
            <div class="snc-header">
                <a href="" class="close-menu menu-link">Close</a>
            </div>
            <?php wp_nav_menu(array(
            'menu'            => 'Primary Nav',
            'container'       => 'nav',
            'container_class' => 'site-nav',
            'menu_class'      => 'sn-level-1',
            'walker'        => new themeslug_walker_nav_menu
            )); ?>
        </div>
        <a class="sh-ico-search search-link" target="_blank" href="#"></a>
        <a href="" class="site-nav-container-screen menu-link">&nbsp;</a>
        <!--Site Nav END-->
    </div><!--inner-wrap END-->
</header>
<!-- woocommerce Menu -->
<?php if(is_woocommerce() || is_page(array('cart', 'checkout'))) : ?>
  <section class="shop-menu">
    <div class="inner-wrap">
      <a href="/products" class="home-link"><img src="<?php bloginfo('template_url'); ?>/img/ico-shop-home.svg" class="blue-home-ico" alt="home" title="home" /><img src="<?php bloginfo('template_url'); ?>/img/ico-shop-home-white.svg" class="white-home-ico" alt="home" title="home" /> Home</a>
      <?php
$args = array(
  //'orderby' => 'category',
  'taxonomy' => 'product_cat',
  'parent' => 0,
  'hide_empty' => 0
  );
$categories = get_categories( $args );
if (is_product()) {
  $product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' ); 
  if ( $product_cats && ! is_wp_error ( $product_cats ) ){

        $single_cat = array_shift( $product_cats );

       $cat =  $single_cat->name; 
} 
}
if (is_product_category()) {
    $cate = get_queried_object();
    $cateID = $cate->term_id;
    //echo $cateID;
    }
foreach ( $categories as $category ) {
    echo '<a ';
    if(($cat==$category->name) || ($cateID==$category->term_id)){ echo 'class="active" '; } 
      echo ' href="' . get_category_link( $category->term_id ) . '">' . $category->name . '</a>';
}
?>
<a href="/cart" class="cart-icon"><img src="<?php bloginfo('template_url'); ?>/img/ico-cart.svg" alt="cart" title="cart">
<?php global $woocommerce;

// get cart quantity
$qty = $woocommerce->cart->get_cart_contents_count(); ?><span><?php echo $qty; ?></span></a>
    </div>
  </section>
  <?php endif; ?> 
<!--Site Content-->
<section class="site-content-wrap">
