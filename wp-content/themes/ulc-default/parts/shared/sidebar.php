<!--Secondary Content-->
<aside class="site-content-secondary col-3">

	<?php if(get_field('custom_featured_img') ): ?>
		<div class="featured-img">
			<?php the_field('custom_featured_img'); ?>
		</div>
	<?php elseif(has_post_thumbnail() ): ?>
		<div class="featured-img">
			<?php the_post_thumbnail(); ?>
		</div>
	<?php endif; ?>

	<!-- Aside CTAs -->
	<?php if(get_field('aside_cta') ): ?>
	<div class="cta-aside">
	<?php the_field('aside_cta'); ?>
	</div>                 
	<?php elseif(get_field('global_aside_cta','option') ): ?>
	<div class="cta-aside">
	<?php the_field('global_aside_cta','option'); ?>
	</div>
	<?php endif; ?>

	<!-- Page Conditionals -->
	<?php if (is_page( '5' ) || '5' == $post->post_parent) : ?>
		<h3 class="scs-title">Capabilities</h3>
    <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Capabilities',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>
     <style>
				.page-item-28 a, .page-item-28 a, .page-item-99 a, .page-item-101, .page-item-105 {font-weight:bold;}
     </style>

  <?php elseif (is_page( '22' ) || '22' == $post->post_parent) : ?>
		<h3 class="scs-title">Resources</h3>
    <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Resources',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>
     <style>
				.page-item-345 a, .page-item-352 a, .page-item-409 a, .page-item-426{font-weight:bold;}
     </style>

  <?php elseif (is_page( '6' ) || '6' == $post->post_parent) : ?>
		<h3 class="scs-title">Industries</h3>
    <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Industries',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>
     <style>
				.page-item-299 a, .page-item-307 a, .page-item-314 a, .page-item-316 a, .page-item-292 a, .page-item-321 a, .page-item-327 a, .page-item-355 a, .page-item-338 a, .page-item-335 a{font-weight:bold;}
     </style>

   <?php elseif ('352' == $post->post_parent) : ?>
		<h3 class="scs-title">Materials</h3>
    <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Materials',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>
     <style>
				.page-item-389 a, .page-item-366 a, .page-item-384 a, .page-item-382 a{font-weight:bold;}
     </style>

  <?php elseif (is_page( '8' ) || '8' == $post->post_parent) : ?>
		<h3 class="scs-title">About Us </h3>
    <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'About',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>
     <style>
				.page-item-448 a, .page-item-553 a, .page-item-170 a{font-weight:bold;}
     </style>

	<?php elseif ('426' == $post->post_parent) : ?>
		<h3 class="scs-title">Galleries</h3>
    <?php 
	  // $postid = get_the_ID();
	   wp_nav_menu(array(
            'menu'            => 'Galleries',
            'container'       => 'nav',
            'container_class' => 'aside-nav',
			//'exclude' => $postid,
            //'menu_class'      => 'aside-nav',
            //'walker'        => new themeslug_walker_nav_menu
        )); ?>
     <style>
				.page-item-432 a, .page-item-434 a, .page-item-430 a, .page-item-436, .page-item-428 {font-weight:bold;}
     </style>
	<?php endif; ?>

	<!-- Additional Aside Content -->
	<?php if(get_field('additional_aside_content') ): ?>
		<?php the_field('additional_aside_content'); ?>
	</div>                 
	<?php elseif(get_field('global_additional_aside_content','option') ): ?>
		<?php the_field('additional_additional_aside_content'); ?>
	<?php endif; ?>
</aside>



