<?php $term_id = get_queried_object()->term_id;
$post_id = 'product_cat_'.$term_id;
if( get_field('cta_display',$post_id) == false): ?>
<div class="product-cta">
<div class="inner-wrap">
<h2>Additional Resources</h2>
<div class="rows-of-3">
<?php if(get_field('product_cta1',$post_id) ): ?>
      
        <?php the_field('product_cta1',$post_id); ?>

    <?php elseif(get_field('product_cta1','option') ): ?>
    
        <?php the_field('product_cta1','option'); ?>
     
     <?php endif; ?>

<?php if(get_field('product_cta2',$post_id) ): ?>
      
        <?php the_field('product_cta2',$post_id); ?>

    <?php elseif(get_field('product_cta2','option') ): ?>
    
        <?php the_field('product_cta2','option'); ?>
     
     <?php endif; ?>

<?php if(get_field('product_cta3',$post_id) ): ?>
      
        <?php the_field('product_cta3',$post_id); ?>

    <?php elseif(get_field('product_cta3','option') ): ?>
    
        <?php the_field('product_cta3','option'); ?>
     
     <?php endif; ?>

</div>
</div>
</div>
<?php endif;?>