<?php if( get_field('resources_display') == false): ?>
<section class="featured-resources">
       <h2 class="fr-heading">Download Our Free Resources</h2>
             <div class="inner-wrap"> 
             <div class="fr-item">
             <?php if(get_field('resources_module_offer_1') ): ?>
      
        <?php the_field('resources_module_offer_1'); ?>

    <?php elseif(get_field('global_resources_module_offer_1','option') ): ?>
    
        <?php the_field('global_resources_module_offer_1','option'); ?>
     
     <?php endif; ?>
      </div>
      <div class="fr-item">
     <?php if(get_field('resources_module_offer_2') ): ?>
      
        <?php the_field('resources_module_offer_2'); ?>

    <?php elseif(get_field('global_resources_module_offer_2','option') ): ?>
    
        <?php the_field('global_resources_module_offer_2','option'); ?>
     
     <?php endif; ?>
      </div>
      <div class="fr-item">
     <?php if(get_field('resources_module_offer_3') ): ?>
      
        <?php the_field('resources_module_offer_3'); ?>

    <?php elseif(get_field('global_resources_module_offer_3','option') ): ?>
    
        <?php the_field('global_resources_module_offer_3','option'); ?>
     
     <?php endif; ?>
      </div>
               
            </div>
    </section>
    <?php endif; ?>