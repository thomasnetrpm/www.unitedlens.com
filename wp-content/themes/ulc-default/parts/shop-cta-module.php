<section class="shop-cta-module">
<div class="inner-wrap">
<?php if(get_field('cta_heading','option')): ?>
	<div class="sc-heading"><h2><?php the_field('cta_heading','option'); ?></h2></div>
<?php endif; ?>	
	<div class="sc-ctas">
	<?php if( have_rows('shop_cta','option') ):

    while ( have_rows('shop_cta','option') ) : the_row(); ?>
		<a href="<?php the_sub_field('cta_url','option'); ?>" class="btn-dark shop-btn"><?php the_sub_field('cta_title','option'); ?></a>
	<?php  endwhile;
endif;
?>	
	</div>
</div>
</section>