<section class="products-section">
  <div class="ps-text">
<h2>Product Categories</h2>
</div>	
<div class="product-item-wrap rows-of-5-ns">
  <?php
    $prod_categories = get_terms( 'product_cat', array(
        'orderby'    => 'name',
        'order'      => 'ASC',
        'hide_empty' => 1
    ));
    foreach( $prod_categories as $prod_cat ) :
        $cat_thumb_id = get_woocommerce_term_meta( $prod_cat->term_id, 'thumbnail_id', true );
        $cat_thumb_url = wp_get_attachment_thumb_url( $cat_thumb_id );
        $term_link = get_term_link( $prod_cat, 'product_cat' );
?>
<a class="product-item" href="<?php echo $term_link; ?>">
<figure class="">
<img src="<?php echo $cat_thumb_url; ?>" alt="<?php echo $prod_cat->name; ?>" />
 <figcaption>
                    <h3 class="home-product-heading"><?php echo $prod_cat->name; ?></h3>                    
                      </figcaption>   
</figure>
</a>
<?php endforeach; wp_reset_query(); ?>
</div>

	
</section>