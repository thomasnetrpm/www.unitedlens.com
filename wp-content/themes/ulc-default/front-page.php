<?php

    /*
        Template Name: Front Page
    */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<section class="site-content" role="main">
    <section class="site-intro-section">
        <div class="inner-wrap">
            <div class="si-content">
                <h1 class="si-title">One Stop Solution</h1>
                <p class="si-blurb">United Lens Company provides competitive pricing, fast delivery, and unparalleled quality on custom fabrication of all of your optical requirements. </p>
                <!--HubSpot Call-to-Action Code -->
                <span class="hs-cta-wrapper" id="hs-cta-wrapper-ee45dd59-d77d-444a-9e02-05637c7e9745">
                    <span class="hs-cta-node hs-cta-ee45dd59-d77d-444a-9e02-05637c7e9745" id="hs-cta-ee45dd59-d77d-444a-9e02-05637c7e9745">
                        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
                        <a href="http://cta-redirect.hubspot.com/cta/redirect/476692/ee45dd59-d77d-444a-9e02-05637c7e9745"  target="_blank" ><img class="hs-cta-img" id="hs-cta-img-ee45dd59-d77d-444a-9e02-05637c7e9745" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/476692/ee45dd59-d77d-444a-9e02-05637c7e9745.png"  alt="Extend the Life of Your Laser Optics: Download Our Guide"/></a>
                    </span>
                    <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                    <script type="text/javascript">
                        hbspt.cta.load(476692, 'ee45dd59-d77d-444a-9e02-05637c7e9745');
                    </script>
                </span>
                <!-- end HubSpot Call-to-Action Code -->
            </div>
        </div>
    </section>
    
   <!--  <section class="products-section">
        <div class="ps-text">
            
                <h2>Online Catalog</h2>
                <p>ULC Ships In Stock Items Same Day*</p>
             
        </div> 
       <div class="product-item-wrap rows-of-4-ns">
       
            <a href="https://iqms.unitedlens.com/WebDirect/Products/Category?categoryId=20" class="product-item">
                <figure class="">
                <img src="<?php bloginfo('template_url'); ?>/img/img-debris-shields.jpg" alt="img31"/>
                <figcaption>
                    <h3>Debris Shields</h3>
                    <p>High Quality and 100% Customizable</p>
                </figcaption>          
            </figure>
            </a>
            <a href="https://iqms.unitedlens.com/WebDirect/Products/Category?categoryId=14" class="product-item">
            <figure class="">
                <img src="<?php bloginfo('template_url'); ?>/img/img-hp-laser-mirrors.jpg" alt="img10"/>
                <figcaption>
                    <h3>High Power <br>Laser Mirrors</h3>
                    <p>High Performance at an Affordable Price.</p>                  
                </figcaption>           
            </figure>
            </a>
            <a href="https://iqms.unitedlens.com/WebDirect/Products/Category?categoryId=12" class="product-item">
                <figure class="">
                <img src="<?php bloginfo('template_url'); ?>/img/img-colored-filter-glass.jpg" alt="img10"/>
                <figcaption>
                    <h3>Colored Filter Glass</h3>
                    <p>Manufactured Using SCHOTT Optical Filter Glass.</p>
                </figcaption>           
            </figure>
            </a>
            <a href="https://iqms.unitedlens.com/WebDirect/Products/Category?categoryId=3" class="product-item">
                <figure class="">
                <img src="<?php bloginfo('template_url'); ?>/img/img-witness-samples.jpg" alt="img10"/>
                <figcaption>
                    <h3>Witness Samples</h3>
                    <p>Large variety of materials in stock.</p>    
                </figcaption>           
            </figure>
            </a>
            
        </div>
    </section> -->
<?php Starkers_Utilities::get_template_parts( array( 'parts/product-categories-module' ) ); ?>
    <section class="capabilities-rotator-section">
        <div class="cr-nav-container clearfix">
           <h2 class="cr-title">Capabilities</h2>
            <nav class="cr-nav">
                <a href="/thin-film-coating-services" class="cr-nav-item active" data-img="<?php bloginfo('template_url'); ?>/img/capabilities-thin-film-coating.png" data-blurb="ULC specializes in custom designed, high performance thin film coatings."><span>Thin Film</span> Coating</a>
                <a href="/grinding-polishing" class="cr-nav-item" data-img="<?php bloginfo('template_url'); ?>/img/grinding-and-polishing.png" data-blurb="ULC employs the latest technology and techniques from high speed double side polishing, single side precision polishing, cylindrical and spherical polishing for all of your custom substrates." ><span>Polishing</span></a>
                <a href="/blank-precision-machining-services" class="cr-nav-item" data-img="<?php bloginfo('template_url'); ?>/img/capabilities-machining.png"  data-blurb="ULC stocks the largest amount of raw material inventory in the industry.  Paired with our superior machine capabilities for almost any configuration, ULC is your one stop shop for machined blanks." ><span>Blank Precision</span> Machining</a>
                <a href="/custom-hand-molding-services" class="cr-nav-item" data-img="<?php bloginfo('template_url'); ?>/img/capabilities-hand-molding.png" data-blurb="ULC has been molding optical materials since 1916. This continues to be a viable option for optics manufacturing today, due to the ability to utilize material in ways that may not be possible with a machined blanking process." ><span>Custom Hand</span> Molding</a>
            </nav>
        </div>
        <div class="cr-window clearfix">
            <div class="cr-content">
                <h3 class="cr-subtitle">Thin Film Coating</h3>
                <p class="cr-blurb">ULC specializes in custom designed, high performance thin film coatings.</p>
                <a href="/thin-film-coating-services" class="cr-link btn-dark">Learn More</a>
            </div>
        </div>
    </section>
</section><!-- site-content END -->
<?php Starkers_Utilities::get_template_parts( array( 'parts/featured-resources' ) ); ?>       

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>