<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

//get_header( 'shop' ); ?>


<section class="shop-intro">
<div class="inner-wrap">	
<?php if(get_field('si_heading', 897) ): ?>
	<h1 class="si-heading"><?php the_field('si_heading', 897); ?> </h1>
<?php endif; ?>
<?php if(get_field('si_subtext', 897) ): ?>
	<p class="si-subtext"> <?php the_field('si_subtext', 897); ?> </p>
<?php endif; ?>  
</div>	
</section>

<?php Starkers_Utilities::get_template_parts( array( 'parts/product-categories-module' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shop-cta-module' ) ); ?>
